<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  	<title>Director | S-Max Advertising</title>
  	<meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no" />
    <meta name="description" content="Welcome to Smax Advertising">
    <meta name="keywords" content="smax,smax advertising,s-max">
    <meta name="author" content="AMS IT">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-1.7.1.min.js"></script>
    <script src="js/script.js"></script>
	<script type="text/javascript">
    (function(b){(function(a){"__CF"in b&&"DJS"in b.__CF?b.__CF.DJS.push(a):"addEventListener"in b?b.addEventListener("load",a,!1):b.attachEvent("onload",a)})(function(){"FB"in b&&"Event"in FB&&"subscribe"in FB.Event&&(FB.Event.subscribe("edge.create",function(a){_gaq.push(["_trackSocial","facebook","like",a])}),FB.Event.subscribe("edge.remove",function(a){_gaq.push(["_trackSocial","facebook","unlike",a])}),FB.Event.subscribe("message.send",function(a){_gaq.push(["_trackSocial","facebook","send",a])}));"twttr"in b&&"events"in twttr&&"bind"in twttr.events&&twttr.events.bind("tweet",function(a){if(a){var b;if(a.target&&a.target.nodeName=="IFRAME")a:{if(a=a.target.src){a=a.split("#")[0].match(/[^?=&]+=([^&]*)?/g);b=0;for(var c;c=a[b];++b)if(c.indexOf("url")===0){b=unescape(c.split("=")[1]);break a}}b=void 0}_gaq.push(["_trackSocial","twitter","tweet",b])}})})})(window);
    /* ]]> */
    </script>
</head>
<body>
<!--==============================header=================================-->
<header>
<div class="bgheader">
	<!--<div class="main">
		<ul class="topmenu fright">
			<li><a href="#">sitemap</a></li>
			<li><a href="#">search</a></li>
			<li class="last"><a href="#">user area</a></li>
		</ul>
	</div>-->
	<div class="main2">
    	<h1><a class="logoa" href="index.php"></a></h1>
    	<nav>
        <ul class="sf-menu">
          <li class="item1"><a href="index.php"><span>home</span></a></li>
          <li class="current item2"><a href="aboutus.php"><span>about</span></a>
             <ul>
              <li><a href="whoweare.php">Who We Are ?</a></li>
              <li><a href="whatwearedoing.php">What We Do ?</a></li>
              <li><a href="#">Board Of Member</a></li>
            </ul>
          </li>
          <li class="item3"><a href="product.php"><span>Products</span></a>
            <ul>
              <li><a href="kashundi.php">Kashundi </a></li>
              <li><a href="ocean_paradise_hotel_resort.php">OCEAN PARADISE (Running) </a></li>
              <li><a href="#">TAVA RESTURANT </a></li>
              <li><a href="#">HABIBS ALVIRAS </a></li>
              <li><a href="#">RESORT BICH BIEW </a></li>
              <li><a href="#">HOTEL MARIN PLAZA </a></li>
            </ul>
          </li>
          <li class="item4"><a href="opportunity.php"><span>opportunity</span></a></li>
          <li class="item5"><a href="contact.php"><span>contacts</span></a></li>
       </ul>
			<div class="clear"></div>
      </nav>
	</div>
		</div>
		
	</div>
	<div class="bg"></div>
</div>
</header>
<!--==============================content================================-->
<div class="contentbg">
<section id="content" class="padcontent">
	
    <div class="container_12" style="margin-bottom:15px;">
    	<div class="wrapper">
        	<h2 style="margin-left:10px; padding-bottom:10px;">Management</h2>
            <p class="padbot" align="justify" style="font-size:13px; color:#FFF; margin-left:10px;">
            	Join with us to creating excitement in career development and be a part of our business asset with dedicated people.
            </p>
            <img src="images/bod.gif" alt="" title="lorem ipsum" style="margin-left:10px;" />
        </div>
    </div>
    
    <div class=" container_12">
		<div class="wrapper">
			
            <div class="grid_6">
				<h2>Director </h2>
				<div class="quotes">
                	<div style="width:150px; height:151px; float:left; border:1px solid; margin-right:15px; text-align:center;">
                    <img src="images/saif.gif" alt="Md. Yeasinul Haque Chy(Tanvi)" title="Md. Yeasinul Hoque Chy(Tanvi)" style="margin-top:3px;" />
                    </div>
					<blockquote class="blockquote" style="font-size:18px; display:inline-block;">Muhammad Saiful Islam (Saif) <br>
                    								Contact No.- 01675760120
                    </blockquote>
                    <div class="clear"></div>
				</div>
			</div>
            
             <div class="grid_6">
				<h2>Director </h2>
				<div class="quotes">
                	<div style="width:150px; height:151px; float:left; border:1px solid; margin-right:15px; text-align:center;">
                    	<img src="images/tanvi.gif" alt="Md. Yeasinul Haque Chy(Tanvi)" title="Md. Yeasinul Hoque Chy(Tanvi)" style="margin-top:3px;" />
                    </div>
					<blockquote class="blockquote" style="font-size:18px; display:inline-block;">Md. Yeasinul Hoque Chy (Tanvi) <br>
                    								Contact No.- 01842808482
                    </blockquote>
                    <div class="clear"></div>
				</div>
			</div>
            
		</div>
	</div>
</section>
<!--<aside style="padding:0px 0px 10px 0px;">
	<div class=" container_12">
		<div class="wrapper" style="padding-top:5px;">
            <div class="grid_4">
            <div>
            	<a href="#" style="display:block;"><img src="images/icon1_01.png" alt="" style="float:left; margin-right:10px;" /></a>
            </div>
            <a href="#"><span style="color:#FFF; font-size:18px; line-height:60px;">Visit us on Facebook</span></a>
            </div>
            
            <div class="grid_4">
            <div>
            	<a href="#" style="display:block;"><img src="images/icon3_01.png" alt="" style="float:left; margin-right:10px;" /></a>
            </div>
            <a href="#"><span style="color:#FFF; font-size:18px; line-height:60px;">Visit us on Twitter</span></a>
            </div>
            
            <div class="grid_4">
            <div>
            	<a href="#" style="display:block;"><img src="images/icon4.png" alt="" style="float:left; margin-right:10px;" /></a>
            </div>
            <a href="#"><span style="color:#FFF; font-size:18px; line-height:60px;">Visit us on Google+</span></a>
            </div>
		</div>
	</div>
</aside>-->
<?php
   
   include('link.php');
   
   ?>
</div>
<!--==============================footer=================================-->
<footer>
    <div class="main">
    	S-Max Advertising &copy; 2013 <a href="http://www.amsitsoft.com" style="float:right;">Design &amp; Developed By AMSIT </a>
    </div>
</footer>
<script type="text/javascript">/* CloudFlare analytics upgrade */</script>
</body>
</html>