<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  	<title>Welcome to S-Max Advertising</title>
  	<meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no" />
    <meta name="description" content="Welcome to Smax Advertising">
    <meta name="keywords" content="smax,smax advertising,s-max">
    <meta name="author" content="AMS IT">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-1.7.1.min.js"></script>
    <script src="js/script.js"></script>
	<script type="text/javascript">
			(function(b){(function(a){"__CF"in b&&"DJS"in b.__CF?b.__CF.DJS.push(a):"addEventListener"in b?b.addEventListener("load",a,!1):b.attachEvent("onload",a)})(function(){"FB"in b&&"Event"in FB&&"subscribe"in FB.Event&&(FB.Event.subscribe("edge.create",function(a){_gaq.push(["_trackSocial","facebook","like",a])}),FB.Event.subscribe("edge.remove",function(a){_gaq.push(["_trackSocial","facebook","unlike",a])}),FB.Event.subscribe("message.send",function(a){_gaq.push(["_trackSocial","facebook","send",a])}));"twttr"in b&&"events"in twttr&&"bind"in twttr.events&&twttr.events.bind("tweet",function(a){if(a){var b;if(a.target&&a.target.nodeName=="IFRAME")a:{if(a=a.target.src){a=a.split("#")[0].match(/[^?=&]+=([^&]*)?/g);b=0;for(var c;c=a[b];++b)if(c.indexOf("url")===0){b=unescape(c.split("=")[1]);break a}}b=void 0}_gaq.push(["_trackSocial","twitter","tweet",b])}})})})(window);
			/* ]]> */
    </script>
</head>
<body>
<!--==============================header=================================-->
<header>
<div class="bgheader">
	<!--<div class="main">
		<ul class="topmenu fright">
			<li><a href="#">sitemap</a></li>
			<li><a href="#">search</a></li>
			<li class="last"><a href="#">user area</a></li>
		</ul>
	</div>-->
	<div class="main2">
    	<h1><a class="logoa" href="index.php"></a></h1>
    	<nav>
        <ul class="sf-menu">
          <li class="current item1"><a href="#"><span>home</span></a></li>
          <li class="item2"><a href="aboutus.php"><span>about</span></a>
             <ul>
              <li><a href="whoweare.php">Who We Are ?</a></li>
              <li><a href="whatwearedoing.php">What We Do ?</a></li>
              <li><a href="bod.php">Board Of Member</a></li>
            </ul>
          </li>
          <li class="item3"><a href="product.php"><span>Products</span></a>
            <ul>
             <li><a href="kashundi.php">Kashundi </a></li>
             <li><a href="ocean_paradise_hotel_resort.php">OCEAN PARADISE (Running) </a></li>
              <li><a href="#">TAVA RESTURANT </a></li>
              <li><a href="#">HABIBS ALVIRAS </a></li>
              <li><a href="#">RESORT BICH BIEW </a></li>
              <li><a href="#">HOTEL MARIN PLAZA </a></li>
            </ul>
          </li>
          <li class="item4"><a href="opportunity.php"><span>opportunity</span></a></li>
          <li class="item5"><a href="contact.php"><span>contacts</span></a></li>
       </ul>
			<div class="clear"></div>
      </nav>
	</div>
	<div class="boxes">
		<div class="box box-1 space">
      <img src="images/1page_img1.jpg" alt="">
			<div class="boxinner">
				<div class="part1">
					<div class="title-1">Solutions</div>
					<div class="text-1">A liquid mixture in which the minor component is uniformly distributed  </div>
				</div>
			</div>
      <div class="part2">
					<div class="inner">
            <div class="title-2">Solutions </div>
            <div class="text-2"><p align="justify">A liquid mixture in which the minor component is uniformly distributed within the major component .</p>
            </div>
            <div class="boxbutton"><a href="#">read more</a></div>
        </div>
      </div>
		</div>
		<div class="box box-2 space">
      <img src="images/1page_img2.jpg" alt="">
			<div class="boxinner">
				<div class="part1">
					<div class="title-1">Confidence </div>
					<div class="text-1">Belief that you have faith on someone or something to be good-work uncountable</div>
				</div>
			</div>
      <div class="part2">
					<div class="inner">
            <div class="title-2">Confidence </div>
            <div class="text-2">
            <p align="justify">Belief that you have faith on someone or something to be good-work uncountable, if you tell someone something in confidence.</p>
            </div>
            <div class="boxbutton"><a href="#">read more</a></div>
        </div>
      </div>
		</div>
		<div class="box box-3" style="border-top:0px;">
      <img src="images/1page_img8.png" alt="">
			<div class="boxinner">
				<div class="part1">
					<div class="title-1">Success</div>
					<div class="text-1">One of the greatest temptations that comes to any leader in the temptation  </div>
				</div>
			</div>
      <div class="part2">
					<div class="inner">
            <div class="title-2">success</div>
            <div class="text-2"><p align="justify">One of the greatest temptations that comes to any leader in the temptation to tell something less than the truth and achievement of something desired, planned or attempted, attributed their to make a success.</p>
            </div>
            <div class="boxbutton"><a href="#">read more</a></div>
          </div>
				</div>
		</div>
	</div>
	<div class="bg"></div>
</div>
</header>
<!--==============================content================================-->
<div class="contentbg">
<section id="content" class="padcontent">
	<div class=" container_12">
		<div class="wrapper">
			<div class="grid_8">
				<h2>Welcome to S - Max Advertising!</h2>
				<p class="padbot" align="justify">The power of  direct marketing we believe in power of speaking one-to-one with consumers, its a dialogue that motivate and compels customers to immediate action,
If you are looking for a more cost effective approach for marketing strategies let the s-max advertising become your new business partner.
From our beginning in July 2010 as a small bangladeshi advertising farm. There was one assign that has helped us to stand out from all other direct sales companies by our commitment. Today that commitment is a strong as ever.</p>
				<a href="#" class="button">Learn More<span></span></a>
			</div>
			<div class="grid_4">
            	<!--<h2>Save Your Maximum</h2>-->
				<div class="quotes">
					<blockquote class="blockquote">Can you imagine 25,000 New customer in a week?</blockquote>
					<span></span>
				</div>
				<div class="comment"></div>
			</div>
            <div class="grid_12" style="margin-top:20px;">
				<div class="caption_frame">
                	<div style="background:url(images/bgkkk.png) no-repeat left; padding-left:40px;">100% Goal Achive </div>
                    <img src="images/goal.jpg" />
                </div>
                <div class="caption_frame margin_right5px margin_left5px">
                	<div style="background:url(images/ddf.png) no-repeat left; padding-left:40px;">We Love Our Customer </div>
                    <img src="images/cs.jpg" />
                </div>
                
                <div class="caption_frame margin_right5px">
                	<div style="background:url(images/ccccs.png) no-repeat left; padding-left:40px;">Customer Satisfaction </div>
                    <img src="images/customerservice.jpg" />
                </div>
                
                <div class="caption_frame">
                	<div style="background:url(images/ccc.png) no-repeat left; padding-left:40px;">24 X 7 Customer Support </div>
                    <img src="images/247.jpg" />
                </div>
			</div>
		</div>
	</div>
</section>
<!--<aside style="padding:0px 0px 10px 0px;">
	<div class=" container_12">
		<div class="wrapper" style="padding-top:5px;">
            <div class="grid_4" style="width:240px;">
            <div>
            	<a href="#" style="display:block;"><img src="images/icon1_01.png" alt="" style="float:left; margin-right:10px;" /></a>
            </div>
            <a href="#"><span style="color:#FFF; font-size:18px; line-height:60px;">Visit us on Facebook</span></a>
            </div>
            
            <div class="grid_4" style="width:240px;">
            <div>
            	<a href="#" style="display:block;"><img src="images/icon3_01.png" alt="" style="float:left; margin-right:10px;" /></a>
            </div>
            <a href="#"><span style="color:#FFF; font-size:18px; line-height:60px;">Visit us on Twitter</span></a>
            </div>
            
            <div class="grid_4" style="width:240px;">
            <div>
            	<a href="#" style="display:block;"><img src="images/icon4.png" alt="" style="float:left; margin-right:10px;" /></a>
            </div>
            <a href="#"><span style="color:#FFF; font-size:18px; line-height:60px;">Visit us on Google+</span></a>
            </div>
            
            <div style="width:172px; height:60px; line-height:27px; text-align:center; float:right; border:1px solid;">
            	<span style="color:#FFF; font-size:18px;">Total site visitors</span>
                <div style="height:20px; width:100px; margin:5px auto 0px; border:1px solid #CCC; text-align:center; line-height:20px; font-size:18px;">20</div>
            </div>
		</div>
	</div>
</aside>-->

	<?php
   
   include('link.php');
   
   ?>

</div>
<!--==============================footer=================================-->
<footer>
    <div class="main">
    	S-Max Advertising &copy; 2013 <a href="http://www.amsitsoft.com" style="float:right;">Design &amp; Developed By AMSIT </a>
    </div>
</footer>
<script type="text/javascript">/* CloudFlare analytics upgrade */</script>
</body>
</html>