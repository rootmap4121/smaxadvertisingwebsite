<?php
	session_start();
	$errmsg_arr = array();
	$errflag = false;
	extract($_POST);
	
	if($name=='')
	{
		$errmsg_arr ='Name Required';
		$errflag = true;	
	}
	
	if($email=='')
	{
		$errmsg_arr ='Email Required';
		$errflag = true;	
	}
	
	
	$page="contact.php";
	
	if($errflag) 
	{
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: ".$page);
		exit();
	}
	
	$fulldate=date('d/m/Y');
	
   $to = "contact@smaxadvertising.com";
   $subject = "Site Contact";
   $message = $message;
   $header = "From:".$email." \r\n";
   $retval = mail($to,$subject,$message,$header);
	
	if($retval)
	{
		$errmsg_arr[] = 'Mail Sent Successfully,S-Max Will Contact With You Soon ....';
		$errflag = true;
		if($errflag) 
		{
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("location: ".$page);
			exit();
		}  
	}
	else
	{
		$errmsg_arr[] = 'Mail Not Sent , Try Again Please';
		$errflag = true;
		if($errflag) 
		{
			$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("location: ".$page);
			exit();
		}  
	}
?>